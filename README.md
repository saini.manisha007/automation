# automation
In this Project I build a rest assured API framework to organize my test code,configurations,utilities and report genration in structured manner.
so I divided my project into packages to organize components logically.
I create dynamic driver machanism to dynamically call test scripts from test script packages and execute them by using java.lang.reflect
I create a common API method package to store global settings such as the endpoints ,request body and other comman parameters. this allows for easy modification of settings without changing the test code.
In validation package i create test classes based on the API endpoint,request body and other methods. I call that methods in this test classes and thrn i use testNG dependency to use assertions for validating API response.
and I implements logging to capture response during test execution.
For the reading data from excel file by using apache poi library I created a class for that and use this data as request body while triggering the API.
for reporting I used extend reports.
the library I used in my project: 
-RestAssured
-JSON
-testNG
-apache poi library
-allure testing
-extends report





