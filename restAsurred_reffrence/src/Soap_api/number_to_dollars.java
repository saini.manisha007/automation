package Soap_api;
import io.restassured.RestAssured; 
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
public class number_to_dollars {

	public static void main(String[] args) {
		//declare base url
				RestAssured.baseURI="https://www.dataaccess.com/";
				
				//declare the request body
				String requestBody="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
						+ "   <soapenv:Header/>\r\n"
						+ "   <soapenv:Body>\r\n"
						+ "      <web:NumberToDollars>\r\n"
						+ "         <web:dNum>200</web:dNum>\r\n"
						+ "      </web:NumberToDollars>\r\n"
						+ "   </soapenv:Body>\r\n"
						+ "</soapenv:Envelope>";
				//trigger the API and fetch the response body
				String responseBody=given().header("Content-Type","text/xml; charset=utf-8").body(requestBody)
				                    .when().post("webservicesserver/NumberConversion.wso")
				                    .then().extract().response().getBody().asString();
				System.out.println(responseBody);
				//extract the response body parameter
				XmlPath xml_res=new XmlPath(responseBody);
				String res_tag=xml_res.getString("NumberToDollarsResult");
				System.out.println(res_tag);
				
				//validate the response body
				Assert.assertEquals(res_tag, "two hundred dollars");
				

	}

}
