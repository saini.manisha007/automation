package Soap_api;
import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
public class number_to_words {

	public static void main(String[] args) {
		//declare base url
				RestAssured.baseURI="https://www.dataaccess.com/";
				
				//declare the request body
				String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
						+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
						+ "  <soap:Body>\r\n"
						+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
						+ "      <ubiNum>100</ubiNum>\r\n"
						+ "    </NumberToWords>\r\n"
						+ "  </soap:Body>\r\n"
						+ "</soap:Envelope>";
				//trigger the API and fetch the response body
				String responseBody=given().header("Content-Type","text/xml; charset=utf-8").body(requestBody)
				                    .when().post("webservicesserver/NumberConversion.wso")
				                    .then().extract().response().getBody().asString();
				System.out.println(responseBody);
				//extract the response body parameter
				XmlPath xml_res=new XmlPath(responseBody);
				String res_tag=xml_res.getString("NumberToWordsResult");
				System.out.println(res_tag);
				
				//validate the response body
				Assert.assertEquals(res_tag, "one hundred ");
				

	}

}
