package driver_pacakage;

import java.io.IOException; 

import validation_package.Get_validation; 
import validation_package.Patch_validation;
import validation_package.Post_validation;
import validation_package.Put_validation;

public class DriverClass2 {
	public static void main(String[] args) throws IOException {
		Post_validation.executor();
		Put_validation.executor();
		Patch_validation.executor();
		Get_validation.executor();
	}

}
