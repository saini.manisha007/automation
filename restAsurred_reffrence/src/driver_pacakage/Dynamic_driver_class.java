package driver_pacakage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import utility_comman_method.execl_data_extractor;

public class Dynamic_driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> tc_execute=execl_data_extractor.Excel_data_reader("test_data", "test_cases", "TC_Name");
		System.out.println(tc_execute); 
		int count=tc_execute.size();
		for (int i=1; i<count; i++) {
			String TC_Name=tc_execute.get(i);
			//System.out.println(TC_Name); 
			//call the test classes on runtime by using java.lang.reflect pacakge.
			Class<?> test_class=Class.forName("validation_package."+TC_Name);
			
			//call the execute method belonging to test class captured in variable TC_Name by using java.lang.reflect class
	        Method execute_method=test_class.getDeclaredMethod("executor");
	        
	        //set the accessibility of method true
	        execute_method.setAccessible(true); 
	        
	        //create the instance of testclass captured in variable name test_class
	        Object instance_of_test_class=test_class.getDeclaredConstructor().newInstance();
	        
	        //execute the test script class fetched in variable test_class
	        execute_method.invoke(instance_of_test_class);
	        
	        
		}
	}

}
