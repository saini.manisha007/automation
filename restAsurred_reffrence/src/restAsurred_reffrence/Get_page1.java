package restAsurred_reffrence;

import io.restassured.RestAssured; 
import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
public class Get_page1 {

	public static void main(String[] args) {
		//declare base url
		RestAssured.baseURI="https://reqres.in/";

		//configure the response body parameter and trigger the API
		String responseBody=given().when().get("api/users?page=1")
				            .then().extract().response().asString();
		System.out.println(responseBody);
		//expected result
		int exp_id[]= {1,2,3,4,5,6};
		String exp_fname[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
		String exp_lname[]={"Bluth","Weaver","Wong","Holt","Morris","Ramos"};
		String exp_email[]={"george.bluth@reqres.in","janet.weaver@reqres.in","emma.wong@reqres.in", "eve.holt@reqres.in","charles.morris@reqres.in","tracey.ramos@reqres.in"};
        //fetch response body parameter
		JSONObject array_res=new JSONObject(responseBody);
		JSONArray Data_Array = array_res.getJSONArray("data");
		System.out.println(Data_Array);
		int count=Data_Array.length();
		System.out.println(count);
		
		for(int i=0; i<count; i++)
		{
		int res_id=Data_Array.getJSONObject(i).getInt("id");
		String res_fname=Data_Array.getJSONObject(i).getString("first_name");
		String res_lname=Data_Array.getJSONObject(i).getString("last_name");
		String res_email=Data_Array.getJSONObject(i).getString("email");
		//validation
		Assert.assertEquals(res_id, exp_id[i]);
		Assert.assertEquals(res_fname, exp_fname[i]);
		Assert.assertEquals(res_lname, exp_lname[i]);
		Assert.assertEquals(res_email, exp_email[i]); 
				
		 
		
	}

}}
