package restAsurred_reffrence;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get_reference {

	public static void main(String[] args) {
		// declare base URL
		RestAssured.baseURI = "https://reqres.in/";
		// configure the response body parameter and trigger API
		String responseBody = given().when().get("api/users?page=2").then().extract().response().asString();

		System.out.println("responseBody is:" + responseBody);
		// expected response
		int exp_id[] = { 7, 8, 9, 10, 11, 12 };
		String exp_fname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_lname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String exp_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		// parse the response body
		JSONObject Array_res = new JSONObject(responseBody);
		JSONArray Data_Array = Array_res.getJSONArray("data");
		System.out.println(Data_Array);
		int count = Data_Array.length();
		System.out.println(count);

		for (int i = 0; i < count; i++) {
			int res_id = Data_Array.getJSONObject(i).getInt("id");
			String res_fname = Data_Array.getJSONObject(i).getString("first_name");
			String res_lname = Data_Array.getJSONObject(i).getString("last_name");
			String res_email = Data_Array.getJSONObject(i).getString("email");
			// validate response body
			Assert.assertEquals(res_id, exp_id[i]);
			Assert.assertEquals(res_fname, exp_fname[i]);
			Assert.assertEquals(res_lname, exp_lname[i]);
			Assert.assertEquals(res_email, exp_email[i]);
		}

	}
}
