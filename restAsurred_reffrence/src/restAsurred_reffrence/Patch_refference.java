package restAsurred_reffrence;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;
public class Patch_refference {

	public static void main(String[] args) {
     //declare base URl
		RestAssured.baseURI="https://reqres.in/";
	 //configure the request parameter and trigger API
		String requestbody= "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}" ;
		String responsebody=given().header("Content-Type","application/json").body(requestbody)
		.when().patch("api/users/2")
		.then().extract().response().asString();
		System.out.println("response body is:" +responsebody);
		//create an object of json path to parse the request then response
		JsonPath jsp_req= new JsonPath(requestbody);
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		LocalDateTime current_date=LocalDateTime.now();
		String exp_date=current_date.toString().substring(0,11);
		
		JsonPath jsp_res= new JsonPath(responsebody);
		String res_name=jsp_res.getString("name");
		String res_job=jsp_res.getString("job");
		String res_date=jsp_res.getString("updatedAt");
		res_date=res_date.substring(0,11);
		//validate response body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);
				
				
						
		

	}

}
