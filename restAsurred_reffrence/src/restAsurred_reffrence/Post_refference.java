package restAsurred_reffrence;

import io.restassured.RestAssured; 
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_refference {

	public static void main(String[] args) {

		// step 1 declare base URL
		RestAssured.baseURI = "https://reqres.in/";
		// step 2 configure the request parameters and trigger the API
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json").body(requestBody).log().all().when()
				.post("api/users").then().log().all().extract().response().asString();
		System.out.println("response body is:" + responsebody);
		// create an object of Json path to parse the request body
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 11);
		// then response body
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is:" + res_job);
		String res_id = jsp_res.getString("id");
		System.out.println("id is:" + res_id);
		String res_date = jsp_res.getString("createdAt");
		res_date = res_date.substring(0, 11);

		System.out.println("date is:" + res_date);
		// validate response body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_date, exp_date);
	}

}
