package utility_comman_method;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class handle_API_logs {
	public static void evidence_creator(File dir_name,String file_name, String endpoint,String requestBody ,String responseBody) throws IOException {
		File newfile=new File(dir_name+"\\"+file_name+".txt" );
		System.out.println("new text file is created to store evidence : "+ newfile.getName()); 
		
		FileWriter datawriter=new FileWriter(newfile);
		datawriter.write("endpoint is: "+endpoint+"\n\n");
		datawriter.write("Request body is: "+requestBody+"\n\n");
		datawriter.write("Response body is: "+responseBody);
		datawriter.close();
	}

}
