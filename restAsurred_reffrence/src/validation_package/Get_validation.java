package validation_package;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray; 
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import api_common_methods.Common_method_handle_API;
import endpoints.Get_endpoints;
import utility_comman_method.handle_API_logs;
import utility_comman_method.handle_directory;

public class Get_validation extends Common_method_handle_API {
@Test
	public static void executor() throws IOException {
		File log_dir=handle_directory.create_log_directory("Get_validation_logs");
		String endpoint = Get_endpoints.get_endpoints_tc1();

		int statusCode = get_statusCode(endpoint);
		System.out.println(statusCode);

		for (int i = 0; i < 5; i++) {
			if (statusCode == 200) {
				String responseBody = get_responseBody(endpoint);
				System.out.println("Request successfull!!");
				System.out.println(responseBody);
				handle_API_logs.evidence_creator(log_dir, "get_validation", endpoint, endpoint, responseBody);
				Get_validation.validator(responseBody); 
				break;
			} else {
				System.out.println("Expected status code not received!!RETRYING...");
			}
		}

	}

	public static void validator(String responseBody) {

		int exp_id[] = { 7, 8, 9, 10, 11, 12 };
		String exp_fname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_lname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String exp_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };

		JSONObject Array_res = new JSONObject(responseBody);
		JSONArray Data_Array = Array_res.getJSONArray("data");
		System.out.println(Data_Array);
		int count = Data_Array.length();
		//System.out.println(count);

		for (int i = 0; i < count; i++) {
			int res_id = Data_Array.getJSONObject(i).getInt("id");
			String res_fname = Data_Array.getJSONObject(i).getString("first_name");
			String res_lname = Data_Array.getJSONObject(i).getString("last_name");
			String res_email = Data_Array.getJSONObject(i).getString("email");

			// validate response body
			Assert.assertEquals(res_id, exp_id[i]);
			Assert.assertEquals(res_fname, exp_fname[i]);
			Assert.assertEquals(res_lname, exp_lname[i]);
			Assert.assertEquals(res_email, exp_email[i]);
		}
	}
}
