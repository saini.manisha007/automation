package validation_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime; 

import org.testng.Assert;
import org.testng.annotations.Test;

import api_common_methods.Common_method_handle_API;
import endpoints.Post_endpoints;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;
import utility_comman_method.handle_API_logs;
import utility_comman_method.handle_directory;

public class Post_validation extends Common_method_handle_API {
@Test
	public static void executor() throws IOException {
		File log_dir=handle_directory.create_log_directory("Post_validation_logs");
		String requestBody = Post_request_repository.post_request_tc1();
		String endpoint = Post_endpoints.post_endpoint_tc1();

		for (int i = 0; i < 5; i++) {
			int statusCode = post_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {
				String responseBody = post_responseBody(requestBody, endpoint);
				System.out.println("Request successfull!!");
				System.out.println(responseBody);
				handle_API_logs.evidence_creator(log_dir, "post_validation", endpoint, requestBody, responseBody);
				Post_validation.validator(requestBody, responseBody);
				break;
			} 
			else {
				System.out.println("Expected status code not received!!RETRYING... ");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String exp_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);

		String res_job = jsp_res.getString("job");
		System.out.println("job is:" + res_job);

		String res_id = jsp_res.getString("id");
		System.out.println("id is:" + res_id);

		String res_date = jsp_res.getString("createdAt");
		res_date = res_date.substring(0, 11);
		System.out.println("date is:" + res_date);

		// validate response body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_date, exp_date);
	}
}
